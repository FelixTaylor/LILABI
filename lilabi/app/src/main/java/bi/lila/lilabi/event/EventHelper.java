/*
    This file is part of LILABI-APP.

    LILABI-APP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LILABI-APP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LILABI-APP.  If not, see <http://www.gnu.org/licenses/>.
 */

package bi.lila.lilabi.event;
import android.util.Log;

import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventHelper {

    /*  Do not change this link! The following methods
        are designed to work with this webpage only.
     */

    private static final String ADDRESS = "https://www.lilabi.net/veranstaltungen/?pno=";
    private static final String LOGTAG = EventHelper.class.getSimpleName();

    private String[] startDate = new String[3];
    private String[] startTime = new String[3];
    private String[] endDate   = new String[3];
    private String[] endTime   = new String[3];
    private DateTime eventStart;
    private DateTime eventEnd;

    private boolean wholeDay;

    /*  GET_EVENTS returns an ArrayList<Event>. To do so the method
        takes the [ADDRESS] link specified above and a integer value
        as parameter which represents the subpage.
     */

    public ArrayList<Event> getEvents(int page) {
        Log.d(LOGTAG, "getEvents");
        ArrayList<Event> events = new ArrayList<>();
        Elements rows = new Elements();
        Element element, secondChild;

        try {
            rows = Jsoup.connect(ADDRESS + page).get().getElementsByTag("tr");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i=1; i<rows.size(); i++) {
            element     = rows.get(i);
            secondChild = element.child(1);
            getEventTimeSpan(element.child(0).text());

            events.add(
                    new Event(
                            secondChild.getElementsByTag("a").text(),                 // TITLE
                            secondChild.getElementsByTag("a").attr("href"), // LINK
                            secondChild.getElementsByTag("i").text(),                 // LOCATION
                            eventStart,
                            eventEnd,
                            wholeDay
                    )
            );
        }

        return events;
    }

   /*   getEventTimeSpan sets the global variables startDate, startTime,
        endDate and endTime to create the two DateTime objects eventStart and
        eventEnd. GET_EVENTS uses these object to set the start and end date
        of an event.
    */

    private void getEventTimeSpan(String rawDate) {
        Log.d(LOGTAG, "getEventTimeSpan");
        wholeDay = false;

        if (rawDate.length() == 23){
            rawDate = rawDate + " ";
        }


        if (rawDate.length() == 24) {
            startDate = rawDate.substring(0, 10).trim().split("/");
            startTime = rawDate.substring(11, 16).trim().split(":");
            endDate   = startDate;
            endTime   = rawDate.substring(18, rawDate.length()).trim().split(":");
        }


        else if (rawDate.length() == 16) {
            startDate  = rawDate.substring(0, 10).trim().split("/");
            startTime  = rawDate.substring(11, 16).trim().split(":");
            endDate    = startDate;
            endTime    = startTime;
            endTime[1] = startTime[1] + 2;
        }


        else if (rawDate.length() > 24) {
            Pattern pattern = Pattern.compile("\\bGanztägig\\b");
            Matcher matcher = pattern.matcher(rawDate);

            /*  TODO: May change code here
                May be I have to change the code so the regular expression is always
                checked for. There could be dates with less characters but with this
                attribute.
             */

            if (matcher.find()) {
                startDate  = rawDate.substring(0, 10).trim().split("/");
                endDate  = rawDate.substring(13, 23).trim().split("/");
                wholeDay = true;

                for (int i=0; i<startTime.length; i++) {
                    startTime[i] = "0";
                    endTime[i]   = "0";
                }
                endTime[0] = "23";

            } else {

                // mehrere tage event
                startDate = rawDate.substring(0, 10).trim().split("/");
                startTime = rawDate.substring(24, 29).split(":");
                endDate   = rawDate.substring(13, 23).trim().split("/");
                endTime   = rawDate.substring(32, rawDate.length()).split(":");
                wholeDay = false;
            }
        }

        eventStart = new DateTime(
                Integer.valueOf(startDate[2]), // YEAR
                Integer.valueOf(startDate[1]), // MONTH
                Integer.valueOf(startDate[0]), // DAY
                Integer.valueOf(startTime[0]), // HOURS
                Integer.valueOf(startTime[1]), // MINUTES
                0);

        eventEnd = new DateTime(
                Integer.valueOf(endDate[2]),   // YEAR
                Integer.valueOf(endDate[1]),   // MONTH
                Integer.valueOf(endDate[0]),   // DAY
                Integer.valueOf(endTime[0]),   // HOURS
                Integer.valueOf(endTime[1]),   // MINUTES
                0);

     /* Log.d("EventHelper", "START_TIME: " + eventStart.toString());
        Log.d("EventHelper", "END_TIME: "   + eventEnd.toString());
        Log.d("EventHelper", "-------");*/
    }
    public String getEventDetail(String url) {
        Elements page = null;

        try {
            page = Jsoup.connect(url).get().getElementsByClass("post-content");

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (page == null) {
            return "<html><body>ERROR: PAGE_NOT_FOUND</body></html>";
        } else {
            return page.outerHtml();
        }
    }
}