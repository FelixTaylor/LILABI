package bi.lila.lilabi;
public class StaticValues {

    /*  INTENT VALUES:
        These values are used to name
        different intent extras.
     */
    public static final String INTENT_EVENT = "event";
    public static final String INTENT_TITLE = "title";

}
