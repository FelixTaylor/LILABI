/*
    This file is part of LILABI-APP.

    LILABI-APP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LILABI-APP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LILABI-APP.  If not, see <http://www.gnu.org/licenses/>.
 */

package bi.lila.lilabi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

import bi.lila.lilabi.actions.ShareEvent;
import bi.lila.lilabi.event.Event;
import bi.lila.lilabi.event.EventHelper;
import bi.lila.lilabi.user.User;
import bi.lila.lilabi.database.DBConstants;
import bi.lila.lilabi.database.DBHandler;

public class EventListActivity extends AppCompatActivity

        implements  View.OnClickListener,
                    TabHost.OnTabChangeListener,
                    TextWatcher,
                    SeekBar.OnSeekBarChangeListener,
                    CompoundButton.OnCheckedChangeListener {

    private class EventListener      implements View.OnClickListener {
        @Override public void onClick(View v) {

            Log.d(LOGTAG + ".EventListener", ".puExtra(event='" + events.get(v.getId()).getLink()  + "')");
            Log.d(LOGTAG + ".EventListener", ".puExtra(url='"   + events.get(v.getId()).toString() + "')");
            Log.d(LOGTAG + ".EventListener", ".puExtra(title='" + events.get(v.getId()).getTitle() + "')");

            startActivity(new Intent(
                    getApplicationContext(),
                    EventDetailActivity.class)
                    .putExtra(StaticValues.INTENT_EVENT, events.get(v.getId()))
            );

        }
    }
    private class ShareClickListener implements View.OnClickListener {
        @Override public void onClick(View v) {

            Log.d(LOGTAG + ".ShareClickListener", ".puExtra(event='" + events.get(v.getId()).toString()  + "')");
            Log.d(LOGTAG + ".ShareClickListener", ".puExtra(title='" + resources.getString(R.string.tv_share_event) + "')");

            startActivity(new Intent(
                    getApplicationContext(),
                    ShareEvent.class)
                    .putExtra(StaticValues.INTENT_EVENT, events.get(v.getId()))
                    .putExtra(StaticValues.INTENT_TITLE, resources.getString(R.string.tv_share_event))
            );

        }
    }

    private static final String LOGTAG = EventListActivity.class.getSimpleName();
    private static final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy");
    private static final DateTimeFormatter timeFormat = DateTimeFormat.forPattern("HH:mm");

    private Dialog settingsDialog,
                   firstStartDialog,
                   updateEventsDialog;

    private Resources        resources;
    private LayoutInflater   inflater;
    private ProgressDialog   progress;
    private DBHandler        handler;
    private User             user;
    private ArrayList<Event> events;
    private EventHelper      eventHelper;
    private TabHost          tabHost;
    private ImageView        search_cancel_button;
    private EditText         search_input;
    private TextView         tv_load_events;


    private int     seekBarValuePagesToLoad;
    //private long    startUpdate, endUpdate;
    private boolean loadAtStart, smallLayoutValue;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.settings_negative:
                Log.d(LOGTAG, "onClick='settings_negative'");
                settingsDialog.cancel();
                break;

            case R.id.update_events_negative:
                Log.d(LOGTAG, "onClick='update_events_negative'");
                updateEventsDialog.cancel();
                loadEventsFromDatabase();
                break;

            case R.id.settings_positive:
                Log.d(LOGTAG, "onClick='settings_positive'");
                saveSettings();
                break;

            case R.id.cancel_search:
                Log.d(LOGTAG, "onClick='cancel_search'");
                search_input.setText("");
                break;

            case R.id.cb_dont_show_again:
                Log.d(LOGTAG, "onClick='cb_dont_show_again'");
                //setUserFirstStart(!user.isFirstStart()); TODO: Reimplement in v1.0.0
                Toast.makeText(getApplicationContext(),
                        resources.getString(R.string.not_yet_implemented),
                        Toast.LENGTH_SHORT).show();
                break;

            case R.id.first_start_negative:
                Log.d(LOGTAG, "onClick='first_start_negative'");
                firstStartDialog.cancel();
                break;

            case R.id.update_events_positive: case R.id.first_start_positive: case R.id.fab: {
                Log.d(LOGTAG, "onClick='update_events_positive' OR 'first_start_positive' OR 'fab'");

                if (updateEventsDialog != null) { updateEventsDialog.cancel(); }
                if (firstStartDialog   != null) { firstStartDialog.cancel(); }

                loadEvents();
                break;
            }
        }
    }
    @Override
    public void onTabChanged(String tabId) {
        // unselected
        for(int i=0; i<tabHost.getTabWidget().getChildCount(); i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(resources.getColor(R.color.colorPrimary));
            ((TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title)).setTextColor(resources.getColor(R.color.white));

            if (i == tabHost.getCurrentTab()) {

                tabHost.getTabWidget().getChildAt(i).setBackgroundColor(resources.getColor(R.color.white));
                ((TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title)).setTextColor(resources.getColor(R.color.colorPrimary));
            }
        }

        // selected
        if (tabHost.getCurrentTab() == 0) {
            (settingsDialog.findViewById(R.id.settings_positive)).setVisibility(View.VISIBLE);
        } else {
            (settingsDialog.findViewById(R.id.settings_positive)).setVisibility(View.GONE);
        }

        Log.d(LOGTAG, "Switched currentTab to: " + tabHost.getCurrentTab());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}
    @Override
    public void afterTextChanged(Editable s) {
        if (s.toString().isEmpty()) {
            search_cancel_button.setAlpha(0F);
            loadEventsFromDatabase();

        } else {
            search_cancel_button.setAlpha(1F);
            String search = s.toString().trim();

            handler.open();
            events = handler.selectEvents(
                    "SELECT * FROM " +
                            DBConstants.EVENT_TABLE_NAME + " WHERE " +
                            DBConstants.EVENT_TITLE      + " LIKE \'%" + search + "%\' OR " +
                            DBConstants.EVENT_LOCATION   + " LIKE \'%" + search + "%\' ORDER BY " +
                            DBConstants.EVENT_START_DATE + ", " + DBConstants.EVENT_TITLE
            );

            handler.close();
            displayEvents();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        seekBarValuePagesToLoad = seekBar.getProgress()+1;
        setLoadEventsTextView(seekBarValuePagesToLoad *25);
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.load_on_startup:
                Log.d(LOGTAG, "onCheckedChanged='load_on_startup'");
                loadAtStart = isChecked;
                break;

            case R.id.smallLayoutValue:
                Log.d(LOGTAG, "onCheckedChanged='smallLayoutValue'");
                smallLayoutValue = isChecked;
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings: {
                Log.d(LOGTAG, "onOptionsItemSelected='action_settings'");
                showSettingsDialog();
                showChangelog();
                initializeTabHost();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(LOGTAG, "onCreate");

        resources   = getResources();
        events      = new ArrayList<>();
        eventHelper = new EventHelper();
        handler     = new DBHandler(EventListActivity.this);
        inflater    = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        handler.open();
        user = handler.getUser();
        handler.close();

        smallLayoutValue     = user.isSmallLayout();
        search_input         = findViewById(R.id.searchbar);
        search_cancel_button = findViewById(R.id.cancel_search);

        search_input.addTextChangedListener(this);
        search_cancel_button.setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);

        loadEventsFromDatabase();

        if (user.isFirstStart()) {
            Log.d(LOGTAG, "IsFirstStart=" + user.isFirstStart());
            showFirstStartDialog();

        } else if (user.isAutoUpdateAtStart()) {
            Log.d(LOGTAG, "isAutoUpdateAtStart=" + user.isAutoUpdateAtStart());
            loadEvents();

        } else if ((user.getLastEventUpdateInMillis() + 86400000) <= System.currentTimeMillis()
                && !user.isFirstStart()) {

            showUpdateEventsDialog();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOGTAG, "onResume");
    }

    private void displayEvents() {
        Log.d(LOGTAG, "displayEvents");

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String date = "";
        View row;

        EventListener      eventListener      = new EventListener();
        ShareClickListener shareClickListener = new ShareClickListener();

        TableLayout table  = findViewById(R.id.event_table);
        table.removeAllViews();

        /*  The counter variable is used
            to reset the row background color
            if the next event is at a new date.
         */
        int counter = 0;
        for (int i=0; i<events.size(); i++) {

            /*  If the new date is not the same as the date of the previous event
                then display a textview with the new date and the name of the day.
             */
            if (!date.equals(events.get(i).getStartDate().toLocalDate().toString())) {
                date = events.get(i).getStartDate().toLocalDate().toString();
                table.addView(makeRowTitle(i, dateFormat));
                counter = 0;
            }


            if (user.isSmallLayout()) {
                row = inflater.inflate(R.layout.item_row_event_small, null);

            } else {
                row = inflater.inflate(R.layout.item_row_event, null);
                ((TextView) row.findViewById(R.id.tv_event_location)).setText(events.get(i).getLocation());
                ((TextView) row.findViewById(R.id.tv_event_end)).setText(
                        String.format(resources.getString(R.string.tv_event_end),
                                dateFormat.print(events.get(i).getEndDate()),
                                timeFormat.print(events.get(i).getEndDate().toLocalTime())
                        )
                );

                ImageView share = row.findViewById(R.id.event_share);
                share.setOnClickListener(shareClickListener);
                share.setId(i);
            }

            ((TextView) row.findViewById(R.id.tv_event_title)).setText(
                    trimTitleLength(events.get(i).getTitle()));

            ((TextView) row.findViewById(R.id.tv_event_time)).setText(
                    String.format(resources.getString(R.string.tv_event_time),
                            timeFormat.print(events.get(i).getStartDate().toLocalTime())
                    )
            );

            if (events.get(i).getStartDate().toLocalDate().isEqual(events.get(i).getEndDate().toLocalDate())) {
                // Event ends at the same day
                (row.findViewById(R.id.hint_multi_day)).setVisibility(View.GONE);
            } else if (!events.get(i).getStartDate().toLocalDate().isEqual(events.get(i).getEndDate().toLocalDate())) {
                // Event does NOT end at the same day
                if (events.get(i).getEndDate().toLocalTime().getHourOfDay() < 12) {
                    // The event does not end before 12 oclock
                    (row.findViewById(R.id.hint_multi_day)).setVisibility(View.GONE);
                }
            }

            /*  If this event takes place over
                one or more full days display
                a tag under the event.
             */

            if (!events.get(i).isWholeDay()) {
                (row.findViewById(R.id.hint_whole_day)).setVisibility(View.GONE);
            }

            if (counter%2 == 0) {
                row.setBackgroundColor(resources.getColor(R.color.table_light));
            }

            row.setId(i);
            row.setOnClickListener(eventListener);
            table.addView(row);
            counter++;
        }
    }
    private void saveSettings() {
        Log.d(LOGTAG, "saveSettings");
        boolean b = user.isSmallLayout();

        user.setPagesToLoad(seekBarValuePagesToLoad);
        user.setAutoUpdateAtStart(loadAtStart);
        user.setSmallLayout(smallLayoutValue);

        boolean validUpdate = updateUser(user);
        String message = validUpdate ?
                resources.getString(R.string.message_settings_saved)    :
                resources.getString(R.string.message_settings_not_saved);


        settingsDialog.cancel();
        Toast.makeText(getApplicationContext(),
                message, Toast.LENGTH_SHORT).show();


        if(b != user.isSmallLayout()) {
            startActivity(new Intent(getApplicationContext(), EventListActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
    private boolean updateUser(User user) {
        Log.d(LOGTAG, "updateUser('" + user.toString() + "')");

        handler.open();
        boolean updated = handler.update(user);
        handler.close();

        return updated;
    }

    private void loadEvents() {
        Log.d(LOGTAG, "loadEvents");

        user.setLastEventUpdateInMillis(System.currentTimeMillis());
        updateUser(user);

        progress  = ProgressDialog.show(EventListActivity.this, resources.getString(R.string.tv_title_loading_events),
                resources.getString(R.string.message_loading_events), true);

        new Thread(getEvents).start();
    }
    private void loadEventsFromDatabase() {
        Log.d(LOGTAG, "loadEventsFromDatabase()");

        handler.open();
        events = handler.selectAllEvents();
        handler.close();
        displayEvents();

    }
    private String trimTitleLength(String title) {
        int maxLength = user.isSmallLayout() ? 30 : 60;
        return title.length() > maxLength
                ? new StringBuilder(title.substring(0,maxLength-3)).append("...").toString()
                : title;
    }

    private void saveEventsToDatabase() {
        Log.d(LOGTAG, "saveEventsToDatabase");

        handler.open();
        handler.deleteEventTable();

        for (int i=0; i<events.size(); i++) {
            events.set(i, handler.insert(events.get(i)));
        }

        handler.close();
    }
    private void setUserFirstStart(boolean firstStart) {
        Log.d(LOGTAG, "setUserFirstStart('" + firstStart + "')");
        this.user.setFirstStart(firstStart);
        updateUser(user);
    }
    private void setLoadEventsTextView(int value) {
        tv_load_events.setText(
                String.format(resources.getString(R.string.tv_load_event_amount),
                        Integer.toString(value)
                )
        );
    }

    private void showSettingsDialog() {
        Log.d(LOGTAG, "showSettingsDialog");

        settingsDialog = new Dialog(EventListActivity.this);
        settingsDialog.setContentView(R.layout.dialog_settings);

        Switch loadAtStart = settingsDialog.findViewById(R.id.load_on_startup);
        loadAtStart.setChecked(user.isAutoUpdateAtStart());
        loadAtStart.setOnCheckedChangeListener(EventListActivity.this);

        Switch useSmallLayout = settingsDialog.findViewById(R.id.smallLayoutValue);
        useSmallLayout.setChecked(user.isSmallLayout());
        useSmallLayout.setOnCheckedChangeListener(EventListActivity.this);

        tv_load_events = settingsDialog.findViewById(R.id.tv_load_events);

        SeekBar sb_load_events = settingsDialog.findViewById(R.id.event_load_input);
        sb_load_events.setProgress(user.getPagesToLoad()-1);
        sb_load_events.setOnSeekBarChangeListener(EventListActivity.this);

        setLoadEventsTextView((sb_load_events.getProgress()+1)*25);

        (settingsDialog.findViewById(R.id.settings_positive)).setOnClickListener(this);
        (settingsDialog.findViewById(R.id.settings_negative)).setOnClickListener(this);
        settingsDialog.show();
    }
    private void showUpdateEventsDialog() {
        Log.d(LOGTAG, "showUpdateEventsDialog");

        updateEventsDialog = new Dialog(EventListActivity.this);
        updateEventsDialog.setContentView(R.layout.dialog_update_event_list);
        (updateEventsDialog.findViewById(R.id.update_events_negative)).setOnClickListener(this);
        (updateEventsDialog.findViewById(R.id.update_events_positive)).setOnClickListener(this);
        updateEventsDialog.show();
    }
    private void showFirstStartDialog() {
        Log.d(LOGTAG, "showFirstStartDialog");

        firstStartDialog = new Dialog(EventListActivity.this);
        firstStartDialog.setContentView(R.layout.dialog_first_start);
        (firstStartDialog.findViewById(R.id.cb_dont_show_again))  .setOnClickListener(EventListActivity.this);
        (firstStartDialog.findViewById(R.id.first_start_positive)).setOnClickListener(EventListActivity.this);
        (firstStartDialog.findViewById(R.id.first_start_negative)).setOnClickListener(this);
        firstStartDialog.show();
    }
    private void showChangelog() {
        Log.d(LOGTAG, "showChangelog");

        TableLayout table = settingsDialog.findViewById(R.id.dialog_table);
        String[] titles   = resources.getStringArray(R.array.array_titles_changelog);

        ArrayList<String[]> notes = new ArrayList<>();
        notes.add(resources.getStringArray(R.array.changelog_functions));
        notes.add(resources.getStringArray(R.array.changelog_bugfix));
        notes.add(resources.getStringArray(R.array.changelog_improvements));
        notes.add(resources.getStringArray(R.array.changelog_design));
        notes.add(resources.getStringArray(R.array.changelog_removed));

        for (int i=0; i<titles.length; i++) {
            if (notes.get(i).length > 0) {
                showUpdates(table, titles[i], notes.get(i));
            }
        }
    }
    private void showUpdates(TableLayout table, String title, String[] notes) {
        TextView changeLog_title = (TextView) inflater.inflate(R.layout.item_list_title, null);
        changeLog_title.setText(title.toUpperCase());

        table.addView(changeLog_title);

        for (String s : notes) {
            View row = inflater.inflate(R.layout.item_row_updates, null);
            ((TextView) row.findViewById(R.id.update_text)).setText(s);
            table.addView(row);
        }
    }

    private void initializeTabHost() {
        Log.d(LOGTAG, "initializeTabHost");
        tabHost = settingsDialog.findViewById(R.id.tab_host);
        tabHost.setup();

        String tabText[] = {resources.getString(R.string.action_settings), resources.getString(R.string.app_version)};
        int layoutIds[]  = {R.id.tab1, R.id.tab2};

        for (int i=0; i<tabText.length; i++) {
            tabHost.addTab(makeTab(tabText[i], layoutIds[i]));

            ((TextView) tabHost.getTabWidget().getChildAt(i)
                    .findViewById(android.R.id.title))
                    .setTextColor(resources.getColor(R.color.table_light));

            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(resources.getColor(R.color.colorPrimary));
        }

        tabHost.setOnTabChangedListener(EventListActivity.this);
        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(resources.getColor(R.color.white)); //selected
        TextView tv = tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        tv.setTextColor(resources.getColor(R.color.colorPrimary));
    }
    private TabHost.TabSpec makeTab(String tabText, int contentId) {
        Log.d(LOGTAG, "makeTab('" + tabText + "'," + contentId + "')");
        TabHost.TabSpec tab = tabHost.newTabSpec(tabText);
        tab.setIndicator(tab.getTag()).setContent(contentId);
        return tab;
    }
    private View makeRowTitle(int id, DateTimeFormatter dateFormat) {
        View row = inflater.inflate(R.layout.item_row_date, null);
        String format = user.isSmallLayout()
                ? resources.getString(R.string.tv_small_date)
                : resources.getString(R.string.tv_date);

        ((TextView) row.findViewById(R.id.tv_date)).setText(
                String.format(format,
                        events.get(id).getStartDate().dayOfWeek().getAsText(),
                        dateFormat.print(events.get(id).getStartDate())
                )
        );

        return row;
    }

    private Runnable getEvents = new Runnable() {
        @Override public void run() {
            Log.d(LOGTAG, "New Runnable: getEvents");
            // startUpdate = System.currentTimeMillis();
            events.clear();


            for (int i=1; i<=user.getPagesToLoad(); i++) {
                events.addAll(eventHelper.getEvents(i));
            }
            // endUpdate = System.currentTimeMillis();

            runOnUiThread(new Runnable() {
                @Override public void run() {
                    Log.d(LOGTAG, "New Runnable: runOnUiThread");
                    if (events.size() <= 0) {
                        Toast.makeText(getApplicationContext(),
                                resources.getString(R.string.message_no_events),
                                Toast.LENGTH_LONG).show();
                    }

                    else {
                       /* Toast.makeText(getApplicationContext(),
                                String.format(resources.getString(
                                        R.string.message_loaded_events_in_time),
                                        events.size(), (endUpdate-startUpdate)/1000),
                                Toast.LENGTH_LONG).show();
                        */
                        displayEvents();
                        saveEventsToDatabase();

                    }

                    progress.dismiss();
                }
            });
        }
    };
}