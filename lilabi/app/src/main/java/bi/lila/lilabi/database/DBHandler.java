package bi.lila.lilabi.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;

import bi.lila.lilabi.event.Event;
import bi.lila.lilabi.user.User;

public class DBHandler {
    private static final String LOGTAG = DBHandler.class.getSimpleName();
    private DBManager dbManager;
    private SQLiteDatabase db;

    public DBHandler(Context context) {
        this.dbManager = new DBManager(context);
        Log.d(LOGTAG, "Ready");
    }
    public void open() {
        this.db = dbManager.getWritableDatabase();
        Log.d(LOGTAG, "The connection to database " + dbManager.getDatabaseName() + " is active.");
    }
    public void close() {
        dbManager.close();
        Log.d(LOGTAG, "Database connection has been closed.");
    }

    // USER METHODS
    public boolean update(User u) {
        int amount = db.update(
                DBConstants.USER_TABLE_NAME,
                makeUserValues(u),
                DBConstants.PARAM_WHERE_USER_ID,
                new String[]{String.valueOf(u.getId())}
        );

        Log.d(LOGTAG, "update(" + u.toString() + ")");
        return amount>0;
    }
    public Event insert(Event event) {
        Log.d(LOGTAG, "insert(" + event.toString() + ")");
        Long id = db.insert(
                DBConstants.EVENT_TABLE_NAME,
                null,
                makeEventValues(event)
        );

        return new Event(id,
                event.getTitle(),
                event.getLink(),
                event.getLocation(),
                event.getStartDate(),
                event.getEndDate(),
                event.isWholeDay());
    }
    private ContentValues makeEventValues(Event event) {
        Log.d(LOGTAG, "makeEventValues(" + event.toString() + ")");
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.EVENT_TITLE,      event.getTitle());
        cv.put(DBConstants.EVENT_LINK,       event.getLink());
        cv.put(DBConstants.EVENT_LOCATION,   event.getLocation());
        cv.put(DBConstants.EVENT_START_DATE, event.getStartDate().getMillis());
        cv.put(DBConstants.EVENT_END_DATE,   event.getEndDate().getMillis());
        cv.put(DBConstants.EVENT_WHOLE_DAY,  event.isWholeDay() ? 1 : 0);
        return cv;
    }
    private Event cursor2Event(Cursor c) {
        long id         = c.getInt(c.getColumnIndex(DBConstants.EVENT_ID));
        String title    = c.getString(c.getColumnIndex(DBConstants.EVENT_TITLE));
        String link     = c.getString(c.getColumnIndex(DBConstants.EVENT_LINK));
        String location = c.getString(c.getColumnIndex(DBConstants.EVENT_LOCATION));
        long startDate  = c.getLong(c.getColumnIndex(DBConstants.EVENT_START_DATE));
        long endDate    = c.getLong(c.getColumnIndex(DBConstants.EVENT_END_DATE));
        int wholeDay    = c.getInt(c.getColumnIndex(DBConstants.EVENT_WHOLE_DAY));

        return new Event(id,title,
                link,location,
                new DateTime(startDate),
                new DateTime(endDate),
                wholeDay==1);
    }
    public ArrayList<Event> selectAllEvents() {
        Log.d(LOGTAG, "selectAllEvents");
        ArrayList<Event> events = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SELECT_EVENT, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SELECT_EVENT);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            events.add(this.cursor2Event(cursor));
            Log.d(LOGTAG, "FOUND: " + events.get(events.size()-1).toString());
            cursor.moveToNext();
        }
        return events;
    }
    public ArrayList<Event> selectEvents(String statement) {
        Log.d(LOGTAG, "selectAllEvents(" + statement + ")");
        ArrayList<Event> events = new ArrayList<>();
        Cursor cursor = db.rawQuery(statement, null);
        Log.d(LOGTAG, "STATEMENT: " + statement);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            events.add(this.cursor2Event(cursor));
            Log.d(LOGTAG, "FOUND: " + events.get(events.size()-1).toString());
            cursor.moveToNext();
        }
        return events;
    }
    public void deleteEventTable() {
        Log.d(LOGTAG, "deleteEventTable");
        db.delete(DBConstants.EVENT_TABLE_NAME, null, null);
    }

    public User getUser() {
        Log.d(LOGTAG, "getUser");
        Cursor cursor = db.rawQuery(DBConstants.SELECT_USER, null);
        cursor.moveToFirst();
        return cursor2User(cursor);
    }
    private User cursor2User(Cursor c) {
        Log.d(LOGTAG, "selectAllEvents(" + c.toString() + ")");
        long id = c.getLong(c.getColumnIndex(DBConstants.USER_ID));
        long lastEventUpdate = c.getLong(c.getColumnIndex(DBConstants.USER_LAST_EVENT_UPDATED));
        int pagesToLoad = c.getInt(c.getColumnIndex(DBConstants.USER_PAGES_TO_LOAD));

        boolean autoUpdate  = c.getInt(c.getColumnIndex(DBConstants.USER_AUTO_UPDATE_AT_START)) == 1;
        boolean isFistStart = c.getInt(c.getColumnIndex(DBConstants.USER_IS_FIRST_START)) == 1;
        boolean isSmallLayout = c.getInt(c.getColumnIndex(DBConstants.USER_SMALL_LAYOUT)) == 1;

        Log.d(LOGTAG, "FOUND: User (" + id + ")");
        return new User(id, pagesToLoad, autoUpdate, isFistStart, lastEventUpdate, isSmallLayout);
    }

    private ContentValues makeUserValues(User u) {
        Log.d(LOGTAG, "selectAllEvents(" + u.toString() + ")");
        ContentValues values = new ContentValues();
        values.put(DBConstants.USER_LAST_EVENT_UPDATED,   u.getLastEventUpdateInMillis());
        values.put(DBConstants.USER_PAGES_TO_LOAD,        u.getPagesToLoad());
        values.put(DBConstants.USER_AUTO_UPDATE_AT_START, u.isAutoUpdateAtStart());
        values.put(DBConstants.USER_IS_FIRST_START,       u.isFirstStart());
        values.put(DBConstants.USER_SMALL_LAYOUT,         u.isSmallLayout());
        return values;
    }
}
