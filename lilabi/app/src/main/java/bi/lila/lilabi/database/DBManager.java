package bi.lila.lilabi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBManager extends SQLiteOpenHelper {
    private static final String LOGTAG = DBManager.class.getSimpleName();

    public DBManager(Context context) {
        super(context, DBConstants.DBNAME, null, DBConstants.DBVERSION);
        Log.d(LOGTAG, "Created DB and opened:" + this.getDatabaseName());
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstants.CREATE_USER_TABLE);
        db.execSQL(DBConstants.CREATE_EVENT_TABLE);
        db.execSQL(DBConstants.INSERT_DEFAULT_USER);
    }

    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Update onUpgrade method with new version.


    }
}
