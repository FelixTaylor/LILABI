package bi.lila.lilabi.database;
public class DBConstants {

    public static final String DBNAME = "lilabi.db";
    public static final int DBVERSION = 2;

    // USER TABLE
    public static final String USER_TABLE_NAME           = "user";
    public static final String USER_ID                   = USER_TABLE_NAME + "_id";
    public static final String USER_PAGES_TO_LOAD        = USER_TABLE_NAME + "_pagesToLoad";
    public static final String USER_AUTO_UPDATE_AT_START = USER_TABLE_NAME + "_autoUpdateAtStart";
    public static final String USER_IS_FIRST_START       = USER_TABLE_NAME + "_isFirstStart";
    public static final String USER_LAST_EVENT_UPDATED   = USER_TABLE_NAME + "_lastEventUpdated";
    public static final String USER_SMALL_LAYOUT         = USER_TABLE_NAME + "_smallLayout";

    // EVENT TABLE
    public static final String EVENT_TABLE_NAME = "event";
    public static final String EVENT_ID         = EVENT_TABLE_NAME + "_id";
    public static final String EVENT_TITLE      = EVENT_TABLE_NAME + "_title";
    public static final String EVENT_LINK       = EVENT_TABLE_NAME + "_link";
    public static final String EVENT_LOCATION   = EVENT_TABLE_NAME + "_location";
    public static final String EVENT_START_DATE = EVENT_TABLE_NAME + "_startDate";
    public static final String EVENT_END_DATE   = EVENT_TABLE_NAME + "_endDate";
    public static final String EVENT_WHOLE_DAY  = EVENT_TABLE_NAME + "_wholeDay";
    public static final String EVENT_BOOKMARK   = EVENT_TABLE_NAME + "_bookmark";

    /* DROP */   public static final String DELETE_EVENT_TABLE   = "DELETE FROM " + EVENT_TABLE_NAME;
                 public static final String DELETE_USER_TABLE    = "DELETE FROM " + USER_TABLE_NAME;
    /* SELECT */ public static final String SELECT_USER          = "SELECT * FROM " + USER_TABLE_NAME  + " WHERE " + USER_ID + "=1";
                 public static final String SELECT_EVENT         = "SELECT * FROM " + EVENT_TABLE_NAME;
                 //public static final String SELECT_FROM_EVENT    = "SELECT * FROM " + EVENT_TABLE_NAME + " WHERE " + EVENT_TITLE + " LIKE \'% ? %\' OR " + EVENT_LOCATION + " LIKE \'% ? %\' ORDER BY " + EVENT_START_DATE + ", " + EVENT_TITLE;
    /* PARAM */  public static final String PARAM_WHERE_USER_ID  = USER_ID + "=?";
                 public static final String PARAM_WHERE_EVENT_ID = USER_ID + "=?";


    // CREATE TABLE
    public static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " +
            USER_TABLE_NAME           + "(" +
            USER_ID                   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_PAGES_TO_LOAD        + " INTEGER DEFAULT 1, " +
            USER_AUTO_UPDATE_AT_START + " INTEGER DEFAULT 0, " +
            USER_IS_FIRST_START       + " INTEGER DEFAULT 0, " +
            USER_SMALL_LAYOUT         + " INTEGER DEFAULT 0, " +
            USER_LAST_EVENT_UPDATED   + " INTEGER)";

    public static final String CREATE_EVENT_TABLE = "CREATE TABLE IF NOT EXISTS " +
            EVENT_TABLE_NAME + "(" +
            EVENT_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            EVENT_TITLE      + " TEXT, " +
            EVENT_LINK       + " TEXT, " +
            EVENT_LOCATION   + " TEXT, " +
            EVENT_START_DATE + " INTEGER, " +
            EVENT_END_DATE   + " INTEGER, " +
            EVENT_BOOKMARK   + " INTEGER DEFAULT 0, " +
            EVENT_WHOLE_DAY  + " INTEGER DEFAULT 0)";

    // INSERT
    public static final String INSERT_DEFAULT_USER = "INSERT INTO " +
            USER_TABLE_NAME           + "("  +
            USER_ID                   + ", " +
            USER_PAGES_TO_LOAD        + ", " +
            USER_AUTO_UPDATE_AT_START + ", " +
            USER_IS_FIRST_START       + ", " +
            USER_SMALL_LAYOUT         + ", " +
            USER_LAST_EVENT_UPDATED   +
            ") VALUES ('1','1','0','1','1','0')";
}
