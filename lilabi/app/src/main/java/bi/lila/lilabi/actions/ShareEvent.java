package bi.lila.lilabi.actions;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import bi.lila.lilabi.event.Event;

public class ShareEvent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Event event = (Event) intent.getSerializableExtra("event");
        String shareTitle = intent.getStringExtra("title");

        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTimeFormatter timeFormat = DateTimeFormat.forPattern("HH:mm");

        StringBuilder message = new StringBuilder("Veranstaltung: ");
        message.append(event.getTitle() + "\n");
        message.append("Begin: " + dateFormat.print(event.getStartDate().toLocalDate())
                + ", " + timeFormat.print(event.getStartDate().toLocalTime()) + "Uhr");
        message.append("\nLink: " + event.getLink());

        startActivity(
                Intent.createChooser(
                        new Intent()
                                .setAction(Intent.ACTION_SEND)
                                .setType("text/plain")
                                .putExtra(Intent.EXTRA_TEXT, message.toString()),
                        shareTitle));

        finish();
    }
}
