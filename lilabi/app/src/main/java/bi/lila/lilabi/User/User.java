package bi.lila.lilabi.user;

public class User {
    private long id;
    private int pagesToLoad;
    private boolean autoUpdateAtStart;
    private boolean isFirstStart;
    private long lastEventUpdateInMillis;
    private boolean smallLayout;

    public User() {
        this.pagesToLoad = 2;
        this.autoUpdateAtStart = false;
    }
    public User(long id, int pagesToLoad, boolean autoUpdateAtStart, boolean isFirstStart, long lastEventUpdateInMillis, boolean smallLayout) {
        this.id = id;
        this.pagesToLoad = pagesToLoad;
        this.autoUpdateAtStart = autoUpdateAtStart;
        this.isFirstStart = isFirstStart;
        this.lastEventUpdateInMillis = lastEventUpdateInMillis;
        this.smallLayout = smallLayout;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public int getPagesToLoad() {
        return pagesToLoad;
    }
    public void setPagesToLoad(int pagesToLoad) {
        this.pagesToLoad = pagesToLoad;
    }

    public boolean isAutoUpdateAtStart() {
        return autoUpdateAtStart;
    }
    public void setAutoUpdateAtStart(boolean autoUpdateAtStart) {
        this.autoUpdateAtStart = autoUpdateAtStart;
    }

    public boolean isFirstStart() {
        return isFirstStart;
    }
    public void setFirstStart(boolean firstStart) {
        isFirstStart = firstStart;
    }

    public long getLastEventUpdateInMillis() {
        return lastEventUpdateInMillis;
    }
    public void setLastEventUpdateInMillis(long lastEventUpdateInMillis) {
        this.lastEventUpdateInMillis = lastEventUpdateInMillis;
    }

    public boolean isSmallLayout() {
        return smallLayout;
    }
    public void setSmallLayout(boolean smallLayout) {
        this.smallLayout = smallLayout;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", pagesToLoad=" + pagesToLoad +
                ", autoUpdateAtStart=" + autoUpdateAtStart +
                ", isFirstStart=" + isFirstStart +
                ", lastEventUpdateInMillis=" + lastEventUpdateInMillis +
                '}';
    }
}
