/*
    This file is part of LILABI-APP.

    LILABI-APP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LILABI-APP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LILABI-APP.  If not, see <http://www.gnu.org/licenses/>.
 */

package bi.lila.lilabi.event;
import android.util.Log;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

public class Event implements Serializable {
	private static final long serialVersionUID = new Random().nextLong();
	private static final String LOGTAG = Event.class.getSimpleName();
	private long id;
	private String title, link, location;
	private DateTime startDate, endDate;
	private boolean wholeDay;


	private Event() {
		Log.d(LOGTAG, "Created new Event");
		this.id		   = 1L;
		this.title 	   = "<TITLE>";
		this.link  	   = "<LINK>";
		this.location  = "<LOCATION>";

		this.startDate = new DateTime();
		this.endDate   = this.startDate;
		this.wholeDay  = false;
	}
	public Event(long id, String title, String link, String location, DateTime startDate, DateTime endDate, boolean wholeDay) {
		this();
		setId(id);
		setTitle(title);
		setLink(link);
		setLocation(location);

		setStartDate(startDate);
		setEndDate(endDate);
		setWholeDay(wholeDay);
	}
	public Event(String title, String link, String location, DateTime startDate, DateTime endDate, boolean wholeDay) {
		this();
		setTitle(title);
		setLink(link);
		setLocation(location);

		setStartDate(startDate);
		setEndDate(endDate);
		setWholeDay(wholeDay);
	}

	@Override public String toString() {
		return this.getClass().getSimpleName() + " [title='" + getTitle() + "', link=" + getLink() + "']";
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	public DateTime getStartDate() {
		return startDate;
	}
	private void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	public DateTime getEndDate() {
		return endDate;
	}
	private void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public String getLocation() {
		return location;
	}
	private void setLocation(String location) {
		this.location = location;
	}

	public boolean isWholeDay() {
		return wholeDay;
	}
	private void setWholeDay(boolean wholeDay) {
		this.wholeDay = wholeDay;
	}
}