package bi.lila.lilabi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import bi.lila.lilabi.actions.ShareEvent;
import bi.lila.lilabi.event.Event;
import bi.lila.lilabi.event.EventHelper;

public class EventDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String LOGTAG = EventDetailActivity.class.getSimpleName();

    private WebView     eventDetailView;
    private String      HTMLDocument;
    private EventHelper eventHelper;
    private Event       event;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                Log.d(LOGTAG + ".onOptionsItemSelected", "clicked='action_share'");
                startActivity(new Intent(
                        getApplicationContext(),
                        ShareEvent.class)
                        .putExtra(StaticValues.INTENT_EVENT, event)
                        .putExtra(StaticValues.INTENT_TITLE, getResources().getString(R.string.tv_share_event))
                );
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_event_detail);

        Log.d(LOGTAG, "onCreate");
        Intent intent = getIntent();
        event = (Event) intent.getSerializableExtra(StaticValues.INTENT_EVENT);

        Toolbar toolbar = findViewById(R.id.toolbar);
        // ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(event.getTitle());
        ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.tv_event_details_title));
        (toolbar.findViewById(R.id.detail_back_button)).setOnClickListener(this);
        setSupportActionBar(toolbar);

        eventHelper = new EventHelper();
        new Thread(getEventDetail).start();
    }

    private Runnable getEventDetail = new Runnable() {
        @Override public void run() {

            HTMLDocument = eventHelper.getEventDetail(event.getLink());
            runOnUiThread(new Runnable() {
                @Override public void run() {

                    eventDetailView = findViewById(R.id.event_web_view);
                    eventDetailView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    eventDetailView.loadDataWithBaseURL(null, HTMLDocument, "text/html", "utf-8", null);
                    ((TextView) findViewById(R.id.event_detail_title)).setText(event.getTitle());
                    findViewById(R.id.event_detail_progress).setVisibility(View.GONE);
                }
            });
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_back_button:
                Log.d(LOGTAG + ".onClick", "clicked='detail_back_button'");
                onBackPressed();
                break;
        }
    }
}